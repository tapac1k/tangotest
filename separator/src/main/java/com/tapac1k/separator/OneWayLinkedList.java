package com.tapac1k.separator;

class OneWayLinkedList {
    private Node head;

    public Node getHead() {
        return head;
    }

    public void put(int data) {
        Node node = new Node(data);
        if(head == null) {
            head = node;
        } else {
            Node last = head;
            while (last.next != null)
                last = last.next;
            last.next = node;
        }
    }

    class Node {
        int data; 
        Node next;
        Node(int d) {
            data = d;
        }
    }

    public static OneWayLinkedList createFromArray(int[] array) {
        OneWayLinkedList list = new OneWayLinkedList();
        for(int element: array) {
            list.put(element);
        }
        return list;
    }
}