package com.tapac1k.separator;

import java.util.LinkedList;

public class ListSeparator {

    /**
     * Separate one-way-linked Integer list to two linked lists
     *
     * @param node one-way-linked Integer list head
     * @return an array of two linked lists. First contain only odd elements from the input,
     * while second only even elements. Both returned lists are in a reversed order.
     */
    LinkedList<Integer>[] separate(OneWayLinkedList.Node node) {
        LinkedList<Integer>[] lists = new LinkedList[2];
        LinkedList<Integer> oddList = new LinkedList<>();
        LinkedList<Integer> evenList = new LinkedList<>();
        lists[0] = oddList;
        lists[1] = evenList;

        while(node != null) {
            if(node.data % 2 == 0) {
                evenList.addFirst(node.data);
            } else {
                oddList.addFirst(node.data);
            }
            node = node.next;
        }

        return lists;
    }

}
