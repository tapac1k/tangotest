package com.tapac1k.separator;

import org.junit.Assert;
import org.junit.Before;

import java.util.LinkedList;

public class ListSeparatorTest {
    private ListSeparator separator;

    @Before
    public void prepare() {
        separator = new ListSeparator();
    }

    @org.junit.Test
    public void separateTest1() {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Integer[] odd = {9, 7, 5, 3, 1};
        Integer[] even = {10, 8, 6, 4, 2};
        testData(array, odd, even);
    }

    @org.junit.Test
    public void separateTest2() {
        int[] array = {1, 1, 1, 2};
        Integer[] odd = {1, 1, 1};
        Integer[] even = {2};
        testData(array, odd, even);
    }

    @org.junit.Test
    public void separateTest3() {
        int[] array = {0};
        Integer[] odd = {};
        Integer[] even = {0};
        testData(array, odd, even);
    }

    @org.junit.Test
    public void separateTest4() {
        int[] array = {};
        Integer[] odd = {};
        Integer[] even = {};
        testData(array, odd, even);
    }

    private void testData(int[] input, Integer[] oddResult, Integer[] evenResult) {
        OneWayLinkedList list = OneWayLinkedList.createFromArray(input);
        LinkedList[] resultLists = separator.separate(list.getHead());

        Assert.assertArrayEquals(oddResult, resultLists[0].toArray());
        Assert.assertArrayEquals(evenResult, resultLists[1].toArray());
    }
}
